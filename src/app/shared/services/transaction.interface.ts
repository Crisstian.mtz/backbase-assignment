export class Transactions {
  /**
   * Transaction category code
   */
  categoryCode: string;
  /**
   * Transaction date
   */
  dates: DateInfo;
  /**
   * Transaction information
   */
  transaction: TransactionsInfo;
  /**
   * Merchant information
   */
  merchant: MerchantInfo;
}

export class DateInfo {
  /**
   * Date
   */
  valueDate: number;
}

export class TransactionsInfo {
  /**
   * Amount information
   */
  amountCurrency: AmountInfo;
  /**
   * Transaction type
   */
  type: string;
  /**
   * Indicator
   */
  creditDebitIndicator: string;
}

export class AmountInfo {
  /**
   * Amount
   */
  amount: string;
  /**
   * Currency code
   */
  currencyCode: string;
}

export class MerchantInfo {
  /**
   * Name
   */
  name: string;
  /**
   * Account number
   */
  accountNumber: string;
}
