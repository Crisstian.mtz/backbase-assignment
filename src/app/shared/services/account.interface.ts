export class AccountInfo {
  /**
   * Account name
   */
  name: string;
  /**
   * Account number
   */
  accountNumber: number;
  /**
   * Account amount
   */
  amountAvailable: number;
}
