import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { AccountService } from './account.service';
import { Transactions, MerchantInfo } from './transaction.interface';

@Injectable({
  providedIn: 'root',
})
export class TransactionService {
  /**
   * Transactions interface
   */
  private transactions: Transactions[] = [];
  /**
   * Subject transactions
   */
  public subjectTransactions = new ReplaySubject<Transactions[]>(0);
  /**
   * Constructor
   */
  constructor(public http: HttpClient, private account: AccountService) {}
  /**
   * Set transactions
   */
  setTransactions() {
    this.http.get('assets/mock/transactions.json').subscribe((resp: any) => {
      this.transactions = resp.data;
      this.subjectTransactions.next(this.transactions);
    });
  }
  /**
   * Get transactions
   */
  getTransactions(): Observable<Transactions[]> {
    return this.subjectTransactions;
  }
  /**
   * Add transaction
   */
  addTransaction(totalAmount: string, merchantInfo: MerchantInfo): boolean {
    if (this.account.validateTransaction(+totalAmount)) {
      this.account.makeTransfer(+totalAmount);
      const mock = {
        categoryCode: this.transactions[
          Math.round(Math.random() * this.transactions.length)
        ].categoryCode,
        dates: {
          valueDate: Date.now(),
        },
        transaction: {
          amountCurrency: {
            amount: totalAmount,
            currencyCode: 'EUR',
          },
          type: 'Online Transfer',
          creditDebitIndicator: 'DBIT',
        },
        merchant: merchantInfo,
      };

      this.transactions.unshift(mock);
      this.subjectTransactions.next(this.transactions);
      return true;
    } else {
      return false;
    }
  }
}
