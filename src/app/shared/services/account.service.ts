import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';

import { AccountInfo } from './account.interface';

@Injectable({
  providedIn: 'root',
})
export class AccountService {
  /**
   * Overdraft
   */
  OVERDRAFT = -500;
  /**
   * Account information
   */
  private accountInfo: AccountInfo = {
    name: 'Free Checking',
    accountNumber: 4692,
    amountAvailable: 5824.76,
  };
  /**
   * Subject account
   */
  public subjectAccount = new ReplaySubject<AccountInfo>(0);
  /**
   * Constructor
   */
  constructor() {}
  /**
   * Set account
   */
  setAccount() {
    this.subjectAccount.next(this.accountInfo);
  }
  /**
   * Get account info
   */
  getAccountInfo(): Observable<AccountInfo> {
    return this.subjectAccount;
  }
  /**
   * Transfer
   */
  makeTransfer(amount: number) {
    this.accountInfo.amountAvailable -= amount;
    this.subjectAccount.next(this.accountInfo);
  }
  /**
   * Validate transaction
   */
  validateTransaction(amount: number): boolean {
    const previewAmount = this.accountInfo.amountAvailable - amount;
    if (this.OVERDRAFT > previewAmount) {
      return false;
    } else {
      return true;
    }
  }
}
