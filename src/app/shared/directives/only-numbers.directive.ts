import { Directive, ElementRef, HostListener, Input, Output, EventEmitter } from '@angular/core';
/**
 * Directive for input to validate an input with only numbers.
 */
@Directive({
  selector: '[onlyNumbers]',
})
export class OnlyNumbersDirective {
  /**
   * Flag to validate the text.
   */
  @Input() onlyNumbers: boolean;
  /**
   * Output to emit the response of the validation.
   */
  @Output() directiveValue = new EventEmitter();
  /**
   * Constructor of the directive.
   */
  constructor(private readonly el: ElementRef) {}
  /**
   * Host listener to detect the event and start the operations.
   */
  @HostListener('input', ['$event']) onKeyDown(event) {
    if (this.onlyNumbers) {
      const initalValue = this.el.nativeElement.value;
      this.el.nativeElement.value = initalValue.replace(/[^0-9]*/g, '');
      if (initalValue !== this.el.nativeElement.value) {
        event.stopPropagation();
        this.directiveValue.emit(this.el.nativeElement.value);
      }
    }
  }
}
