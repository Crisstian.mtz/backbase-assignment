import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BalanceComponent } from './balance.component';

import { OnlyNumbersModule } from 'src/app/shared/directives/only-numbers.module';

@NgModule({
  declarations: [BalanceComponent],
  imports: [CommonModule, OnlyNumbersModule],
  exports: [BalanceComponent],
})
export class BalanceModule {}
