import { Component, OnInit } from '@angular/core';

import { AccountService } from '../../services/account.service';
import { AccountInfo } from '../../services/account.interface';

@Component({
  selector: 'app-balance',
  templateUrl: './balance.component.html',
  styleUrls: ['./balance.component.scss'],
})
export class BalanceComponent implements OnInit {
  /**
   * Account information interface
   */
  balance: AccountInfo;
  /**
   * Constructor
   */
  constructor(private accountService: AccountService) {}
  /**
   * ngOninit
   */
  ngOnInit(): void {
    this.accountService.getAccountInfo().subscribe((accountInfo) => {
      this.balance = accountInfo;
    });
  }
}
