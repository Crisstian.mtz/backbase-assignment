export interface Merchant {
  /**
   * Merchant name
   */
  name: string;
  /**
   * Merchant account number
   */
  accountNumber: string;
}
