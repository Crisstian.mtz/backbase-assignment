import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Merchant } from './autocomplete.interface';
import { AutocompleteInit } from './autocomplete.fixture';

@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.scss'],
})
export class AutocompleteComponent implements OnInit {
  /**
   * Event Output
   */
  @Output() optionSelected = new EventEmitter();
  /**
   * Merchant Input
   */
  data: Merchant[] = AutocompleteInit;
  /**
   * Constructor
   */
  constructor() {}
  /**
   * ngOninit
   */
  ngOnInit() {}
  /**
   * Select option
   */
  selectOption($event) {
    this.optionSelected.emit($event);
  }
}
