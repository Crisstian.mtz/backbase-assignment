import { Merchant } from './autocomplete.interface';

const object1: Merchant = {
  name: 'Backbase',
  accountNumber: 'SI64397745065188826',
};

const object2: Merchant = {
  name: 'The Tea Lounge',
  accountNumber: 'SI64397745065188826',
};
const object3: Merchant = {
  name: 'Texaco',
  accountNumber: 'SI64397745065188826',
};
const object4: Merchant = {
  name: 'Amazon Online Store',
  accountNumber: 'SI64397745065188826',
};
const object5: Merchant = {
  name: '7-Eleven',
  accountNumber: 'SI64397745065188826',
};
const object6: Merchant = {
  name: 'H&M Online Store',
  accountNumber: 'SI64397745065188826',
};
const object7: Merchant = {
  name: 'Jerry Hildreth',
  accountNumber: 'SI64397745065188826',
};
const object8: Merchant = {
  name: 'Lawrence Pearson',
  accountNumber: 'SI64397745065188826',
};
const object9: Merchant = {
  name: 'Whole Foods',
  accountNumber: 'SI64397745065188826',
};
const object10: Merchant = {
  name: 'Southern Electric Company',
  accountNumber: 'SI64397745065188826',
};

export const AutocompleteInit: Merchant[] = [
  object1,
  object2,
  object3,
  object4,
  object5,
  object6,
  object7,
  object8,
  object9,
  object10,
];
