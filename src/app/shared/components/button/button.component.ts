import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent implements OnInit {
  /**
   *Input button style
   */
  @Input() style;
  /**
   * Output event
   */
  @Output() onClickEvent = new EventEmitter();
  /**
   * Constructor
   */
  constructor() {}
  /**
   * ngOninit
   */
  ngOnInit(): void {}
  /**
   * Click event emitter
   */
  public isClicked() {
    this.onClickEvent.emit(true);
  }
}
