import { Component, OnInit } from '@angular/core';

import { TransactionService } from 'src/app/shared/services/transaction.service';
import { Transactions } from 'src/app/shared/services/transaction.interface';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit {
  /**
   * Transactions interface
   */
  data: Transactions[] = [];
  /**
   * Constructor
   */
  constructor(private transaction: TransactionService) {}
  /**
   * ngOninit
   */
  ngOnInit(): void {
    this.transaction.getTransactions().subscribe((transactions) => {
      this.data = transactions;
    });
  }
}
