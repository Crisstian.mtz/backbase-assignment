export class Table {
  /**
   * Transaction category
   */
  category: string;
  /**
   * Transaction date
   */
  date: string;
  /**
   * Account logo
   */
  logo: string;
  /**
   * Transaction beneficiary
   */
  beneficiary: string;
  /**
   * Transaction pay mode
   */
  payMode: string;
  /**
   * Transaction amount
   */
  amount: number;
}
