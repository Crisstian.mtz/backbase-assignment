export enum ModalType {
  /**
   * Modal summary type
   */
  Summary = 'Summary',
  /**
   * Modal confirmation type
   */
  Confirmation = 'Confirmation',
  /**
   * Modal error type
   */
  Error = 'Error',
}
