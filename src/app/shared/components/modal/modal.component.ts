import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { ModalType } from './modal.enum';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {
  /**
   * Output modal close
   */
  @Output() modalClose = new EventEmitter();
  /**
   * Output continue
   */
  @Output() Continue = new EventEmitter();
  /**
   * Input modal type
   */
  @Input() type: ModalType;
  /**
   * Input transaction details
   */
  @Input() details?: { name: string; amount: string };
  /**
   * Modal type
   */
  public typeModal = ModalType;
  /**
   * Constructor
   */
  constructor() {}
  /**
   * ngOninit
   */
  ngOnInit(): void {}
  /**
   * Close modal
   */
  closeModal() {
    this.modalClose.emit(true);
  }
  /**
   * Continue
   */
  onContinue() {
    this.Continue.emit(true);
  }
}
