import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputTextComponent } from './input-text.component';
import { FormsModule } from '@angular/forms';

import { OnlyNumbersModule } from 'src/app/shared/directives/only-numbers.module';

@NgModule({
  declarations: [InputTextComponent],
  imports: [CommonModule, OnlyNumbersModule, FormsModule],
  exports: [InputTextComponent],
})
export class InputTextModule {}
