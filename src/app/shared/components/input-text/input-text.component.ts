import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-input-text',
  templateUrl: './input-text.component.html',
  styleUrls: ['./input-text.component.scss'],
})
export class InputTextComponent implements OnInit {
  /**
   * Input only numbers
   */
  @Input() onlyNumbers: boolean;
  /**
   * Input placeholder
   */
  @Input() placeholder: string;
  /**
   * Input value
   */
  @Input() value: string;
  /**
   * Output event
   */
  @Output() eventKeyPress = new EventEmitter();
  /**
   * Constructor
   */
  constructor() {}
  /**
   * ngOninit
   */
  ngOnInit(): void {}
  /**
   * Is key press
   */
  public isKeyPress(event): void {
    this.eventKeyPress.emit({ event, value: this.value });
  }
  /**
   * Set value
   */
  public setValue(event): void {
    this.value = event;
  }
}
