import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'orderAmount',
})
export class OrderAmountPipe implements PipeTransform {
  transform([...value]: any, asc: boolean = true): string {
    if (asc) {
      return value.sort((a, b) => {
        if (
          a.transaction.amountCurrency.amount >
          b.transaction.amountCurrency.amount
        ) {
          return 1;
        }
        if (
          a.transaction.amountCurrency.amount <
          b.transaction.amountCurrency.amount
        ) {
          return -1;
        }
        return 0;
      });
    }

    return value.sort((a, b) => {
      if (
        a.transaction.amountCurrency.amount >
        b.transaction.amountCurrency.amount
      ) {
        return -1;
      }
      if (
        a.transaction.amountCurrency.amount <
        b.transaction.amountCurrency.amount
      ) {
        return 1;
      }
      return 0;
    });
  }
}
