import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TransactionsRoutingModule } from './transactions-routing.module';
import { TransactionsComponent } from './transactions.component';
import { MakeATransferModule } from './shared/make-a-transfer/make-a-transfer.module';
import { RecentTransactionsModule } from './shared/recent-transactions/recent-transactions.module';

@NgModule({
  declarations: [TransactionsComponent],
  imports: [
    CommonModule,
    TransactionsRoutingModule,
    MakeATransferModule,
    RecentTransactionsModule,
  ],
})
export class TransactionsModule {}
