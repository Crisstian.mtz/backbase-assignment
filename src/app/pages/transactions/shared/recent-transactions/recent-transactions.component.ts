import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-recent-transactions',
  templateUrl: './recent-transactions.component.html',
  styleUrls: ['./recent-transactions.component.scss'],
})
export class RecentTransactionsComponent implements OnInit {
  /**
   * Constructor
   */
  constructor() {}
  /**
   * ngOninit
   */
  ngOnInit(): void {}
}
