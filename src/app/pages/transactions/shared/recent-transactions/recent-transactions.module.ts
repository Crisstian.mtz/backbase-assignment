import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from '../../../../shared/components/button/button.module';
import { InputTextModule } from '../../../../shared/components/input-text/input-text.module';
import { TableModule } from '../../../../shared/components/table/table.module';
import { RecentTransactionsComponent } from './recent-transactions.component';

@NgModule({
  declarations: [RecentTransactionsComponent],
  imports: [CommonModule, ButtonModule, InputTextModule, TableModule],
  exports: [RecentTransactionsComponent],
})
export class RecentTransactionsModule {}
