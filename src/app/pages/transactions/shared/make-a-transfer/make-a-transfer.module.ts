import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from '../../../../shared/components/button/button.module';
import { InputTextModule } from '../../../../shared/components/input-text/input-text.module';
import { MakeATransferComponent } from './make-a-transfer.component';
import { BalanceModule } from '../../../../shared/components/balance/balance.module';
import { ModalModule } from '../../../../shared/components/modal/modal.module';
import { AutocompleteModule } from '../../../../shared/components/autocomplete/autocomplete.module';

@NgModule({
  declarations: [MakeATransferComponent],
  imports: [
    CommonModule,
    ButtonModule,
    InputTextModule,
    BalanceModule,
    ModalModule,
    AutocompleteModule,
  ],
  exports: [MakeATransferComponent],
})
export class MakeATransferModule {}
