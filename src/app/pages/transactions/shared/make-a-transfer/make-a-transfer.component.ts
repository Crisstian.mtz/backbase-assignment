import { Component, OnInit } from '@angular/core';

import { TransactionService } from 'src/app/shared/services/transaction.service';
import { ModalType } from 'src/app/shared/components/modal/modal.enum';

@Component({
  selector: 'app-make-a-transfer',
  templateUrl: './make-a-transfer.component.html',
  styleUrls: ['./make-a-transfer.component.scss'],
})
export class MakeATransferComponent implements OnInit {
  /**
   * Amount
   */
  amount: string;
  /**
   * Modal flag
   */
  modalFlag: boolean = false;
  /**
   * Modal type
   */
  type: ModalType;
  /**
   * Transaction detail
   */
  transactionDetails: { name: string; amount: string };
  /**
   * Destiny account
   */
  destinyAccount: string;
  /**
   * Constructor
   */
  constructor(private transaction: TransactionService) {}
  /**
   * ngOninit
   */
  ngOnInit(): void {}
  /**
   * Send transaction
   */
  sendTransaction($event) {
    if ($event && +this.amount > 0) {
      this.modalFlag = true;
      this.transactionDetails = {
        name: this.destinyAccount,
        amount: this.amount,
      };
      this.type = ModalType.Summary;
    }
  }
  /**
   * Amount
   */
  valueAmount($event) {
    this.amount = $event.value;
  }
  /**
   * Confirm transaction
   */
  confirmTransaction($event) {
    if ($event) {
      const result = this.transaction.addTransaction(this.amount, {
        name: this.destinyAccount,
        accountNumber: 'SI64397745065188826',
      });
      if (result) {
        this.type = ModalType.Confirmation;
      } else {
        this.type = ModalType.Error;
      }
    }
  }
  /**
   * Option selected
   */
  optionSelected($event) {
    this.destinyAccount = $event;
  }
}
