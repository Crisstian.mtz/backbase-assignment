import { Component, OnInit } from '@angular/core';

import { TransactionService } from 'src/app/shared/services/transaction.service';
import { AccountService } from 'src/app/shared/services/account.service';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss'],
})
export class TransactionsComponent implements OnInit {
  /**
   * Constructor
   */
  constructor(
    private transaction: TransactionService,
    private accountService: AccountService
  ) {
    this.transaction.setTransactions();
    this.accountService.setAccount();
  }
  /**
   * ngOninit
   */
  ngOnInit(): void {}
}
